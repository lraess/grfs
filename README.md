# GRFS

## Gaussian Random Field Simulator

Copyright (C) 2018  Ludovic Raess, Dmitriy Kolyukhin and Alexander Minakov.

GRFS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GRFS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GRFS. If not, see <http://www.gnu.org/licenses/>.

### Current version of GRFS can be found at:

For the Julia version, see https://github.com/luraess/ParallelRandomFields.jl

https://bitbucket.org/lraess/grfs/

### GRFS was released in Computers & Geosciences

```
@article{randomfield_rass_2019,
	title = "Efficient parallel random field generator for large 3-D geophysical problems",
	journal = "Computers & Geosciences",
 	volume = "131",
	pages = "158 - 169",
	year = "2019",
	issn = "0098-3004",
	doi = "https://doi.org/10.1016/j.cageo.2019.06.007",
	url = "http://www.sciencedirect.com/science/article/pii/S0098300418309944",
	author = "Ludovic Räss and Dmitriy Kolyukhin and Alexander Minakov",
	keywords = "Geophysics, Geostatistics, Seismology, Computational methods, Parallel and high-performance computing",
}
```

### Distributed software, directory content:

- `GRFS_exp.m` Gaussian Random Field with exponential covariance generation routine based on a MATLAB serial implementation.
- `GPU_GRFS_exp.cu` GPU-based parallel implementation of the GRFS_exp algorithm.
- `GRFS_gauss.m` Gaussian Random Field with Gaussian covariance generation routine based on a MATLAB serial implementation.
- `GPU_GRFS_gauss.cu` GPU-based parallel implementation of the GRFS_gauss algorithm.
- `cuda_scientific.h` Supporting function library for the GPU application.
- `vizme.m` MATLAB-based visualisation script for the GPU codes.

### QUICK START:

- MATLAB: select the routine and enjoy!
- CUDA C: compile the GPU_GRFS.cu routine on a system hosting an Nvidia GPU using the compilation line displayed on the top line of the .cu file. Run it (./a.out) and use the vizme.m MATLAB script to visualise the output. 

#### Contact: ludovic.rass@gmail.com
